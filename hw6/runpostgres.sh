#!/usr/bin/env bash

docker run -d -it -e POSTGRES_PASSWORD=password --name ml-pg -p 5432:5432 postgres:latest
sleep 5 # в зависимости от конфигурации компьютера контейнер с базой стартует разное время
docker cp database.dump ml-pg:/tmp
docker exec ml-pg pg_restore -U postgres -d postgres /tmp/database.dump

# выключить базу
# docker stop ml-pg && docker rm ml-pg