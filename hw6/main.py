import psycopg2
import pandas
from decimal import Decimal

HOST = "localhost"
PORT = 5432
DBNAME = "postgres"
USER = "postgres"
PASSWORD = "password"

conn = psycopg2.connect(
    host=HOST,
    port=PORT,
    dbname=DBNAME,
    user=USER,
    password=PASSWORD)


def mapp(arg):
    if type(arg) is Decimal:
        return int(arg)
    elif type(arg) is str:
        if arg == "пенсионер" or arg == "работает":
            return 1
        elif arg == "не пенсионер" or arg == "не работает":
            return 0
        else:
            return arg
    else:
        return arg


class TargetData:


    def __init__(self):
        self.agreement_rk = None
        self.target = None
        self.age = None
        self.socstatus_work_fl = None
        self.socstatus_pens_fl = None
        self.gender = None
        self.childs_total = None
        self.dependants = None
        self.personal_income = None
        self.loan_num_total = None
        self.loan_num_closed = None

    def __init__(self, ar, tr, ag, socwork, socpens, gen, ct, deps, inc, tl, tcl):
        self.agreement_rk = mapp(ar)
        self.target = mapp(tr)
        self.age = mapp(ag)
        self.socstatus_work_fl = mapp(socwork)
        self.socstatus_pens_fl = mapp(socpens)
        self.gender = mapp(gen)
        self.childs_total = mapp(ct)
        self.dependants = mapp(deps)
        self.personal_income = mapp(inc)
        self.loan_num_total = mapp(tl)
        self.loan_num_closed = mapp(tcl)



def exec_query(query):
    try:
        with conn.cursor() as cursor:
            cursor.execute(query)
            results = cursor.fetchall()
    finally:
        conn.close()
    return results


# Описание модели
#  AGREEMENT_RK — уникальный идентификатор объекта в выборке;
# - TARGET — целевая переменная: отклик на маркетинговую кампанию (1 — отклик был зарегистрирован, 0 — отклика не было);
# - AGE — возраст клиента;
# - SOCSTATUS_WORK_FL — социальный статус клиента относительно работы (1 — работает, 0 — не работает);
# - SOCSTATUS_PENS_FL — социальный статус клиента относительно пенсии (1 — пенсионер, 0 — не пенсионер);
# - GENDER — пол клиента (1 — мужчина, 0 — женщина);
# - CHILD_TOTAL — количество детей клиента;
# - DEPENDANTS — количество иждивенцев клиента;
# - PERSONAL_INCOME — личный доход клиента (в рублях);
# - LOAN_NUM_TOTAL — количество ссуд клиента;
# - LOAN_NUM_CLOSED — количество погашенных ссуд клиента.

clients_main_query = """
select d_clients.*,
d_agreement.agreement_rk,
d_agreement.target,
d_work."comment" as workstatus,
d_pens."comment" as pensstatus,
d_salary.personal_income,
tl.t_loans,
ctl.ct_loans
from d_clients
left join d_agreement on d_clients.id = d_agreement.id_client 
left join d_work on d_clients.socstatus_work_fl = d_work.id
left join d_pens on d_clients.socstatus_pens_fl  = d_pens.id
left join d_salary on d_clients.id = d_salary.id_client
left join (select id_client, count(id_loan) as t_loans  from d_loan group by id_client) tl
on tl.id_client = d_clients.id
left join (select d_loan.id_client, count(d_loan.id_loan) as ct_loans  from d_loan
left join d_close_loan on d_loan.id_loan = d_close_loan.id_loan 
where closed_fl = 1 group by d_loan.id_client) ctl on ctl.id_client = d_clients.id;
"""

d_clients = exec_query(clients_main_query)

data_objects = []

for dclient in d_clients:
    data_objects.append(
        TargetData(
            ar=dclient[14],
            tr=dclient[15],
            ag=dclient[1],
            socwork=dclient[16],
            socpens=dclient[17],
            gen=dclient[2],
            ct=dclient[5],
            deps=dclient[6],
            inc=dclient[18],
            tl=dclient[19],
            tcl=dclient[20]
        )
    )

target_dicts = [client.__dict__ for client in data_objects]

df = pandas.DataFrame.from_records(target_dicts)

df.to_csv('clients.csv')

print()
